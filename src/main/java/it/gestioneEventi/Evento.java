package it.gestioneEventi;

import java.util.Date;

public class Evento {
    private int id;
    private String nome;
    private Date data;
    private Location location;

    public Evento(int id, String nome, Date data, Location location) {
        this.id = id;
        this.nome = nome;
        this.data = data;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
