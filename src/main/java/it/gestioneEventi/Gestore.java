package it.gestioneEventi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Gestore {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/gestione_eventi";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "rootroot";

    private static int indice = 1;

    public static void inserisciPrenotazione(int id, int utente, int evento, int posto) {
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "INSERT INTO gestione_eventi.prenotazioni VALUES (?, ?, ?, ?)";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, String.valueOf(id));
                stmt.setString(2, String.valueOf(utente));
                stmt.setString(3, String.valueOf(evento));
                stmt.setString(4, String.valueOf(posto));
                int rowsInserted = stmt.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("Dati inseriti correttamente nella tabella.");
                } else {
                    System.out.println("Nessuna riga inserita nella tabella.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void inserisciUtente(int id, String nome, String email) {
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "INSERT INTO gestione_eventi.utenti VALUES (?, ?, ?)";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, String.valueOf(id));
                stmt.setString(2, nome));
                stmt.setString(3, email);

                int rowsInserted = stmt.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("Dati inseriti correttamente nella tabella.");
                } else {
                    System.out.println("Nessuna riga inserita nella tabella.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void inserisciEvento(int id, String nome, String descrizione, String data, int location) {
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "INSERT INTO gestione_eventi.eventi VALUES (?, ?, ?, ?, ?)";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, String.valueOf(id));
                stmt.setString(2, nome);
                stmt.setString(3, descrizione);
                stmt.setString(4, data);
                stmt.setString(5, String.valueOf(location));

                int rowsInserted = stmt.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("Dati inseriti correttamente nella tabella.");
                } else {
                    System.out.println("Nessuna riga inserita nella tabella.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
